﻿using Microsoft.Practices.Prism.Mvvm;

namespace MarkupFormatter.MarkupItems
{
    public abstract class MarkupItem : BindableBase
    {
        public MarkupItem()
        {

        }

        private string description;
        private object value;
        private string markupTemplate;

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
                OnPropertyChanged(() => Description);
            }
        }

        public object Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
                OnPropertyChanged(() => Value);
            }
        }

        public string MarkupTemplate
        {
            get
            {
                return markupTemplate;
            }

            set
            {
                markupTemplate = value;
                OnPropertyChanged(() => MarkupTemplate);
            }
        }
    }
}