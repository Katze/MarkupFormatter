﻿using MarkupFormatter.MarkupItems;
using Microsoft.Practices.Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MarkupFormatter
{
    public class MarkupTemplate : BindableBase
    {
        public MarkupTemplate()
        {
            Items = new ObservableCollection<MarkupItem>();
        }

        private ObservableCollection<MarkupItem> items;
        private string templateName;

        public ObservableCollection<MarkupItem> Items
        {
            get
            {
                return items;
            }

            set
            {
                items = value;
            }
        }

        public string TemplateName
        {
            get
            {
                return templateName;
            }

            set
            {
                templateName = value;
            }
        }
    }
}
