﻿using MarkupFormatter.MarkupItems;
using Microsoft.Practices.Prism.Mvvm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MarkupFormatter
{
    public class MainWindowViewModel : BindableBase
    {
        public MainWindowViewModel()
        {
            Templates = new ObservableCollection<MarkupTemplate>();

            DirectoryInfo di = new DirectoryInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Templates");
            if (!di.Exists)
                di.Create();
            if (!di.EnumerateFiles().Any(fi => fi.Name == "ExampleTemplate"))
            {
                FileInfo fi = new FileInfo(di.FullName + @"\ExampleTemplate.json");
                if (!fi.Exists || File.ReadAllLines(fi.FullName).Length <= 5)
                {
                    FileStream fs = fi.Create();
                    fs.Close();
                    MarkupTemplate exTemplate = new MarkupTemplate() { TemplateName = "ExampleTemplate" };
                    exTemplate.Items.Add(new CheckBoxMarkupItem() { Description = "Example description", Value = false, MarkupTemplate = "RandomMarkup%value%" });
                    exTemplate.Items.Add(new TextBoxMarkupItem() { Description = "Example description", Value = "Example text", MarkupTemplate = "RandomMarkup%value%" });
                    exTemplate.Items.Add(new MultiLineTextBoxMarkupItem() { Description = "Example description", Value = "Example text 2", MarkupTemplate = "RandomMarkup%value%" });
                    File.WriteAllText(fi.FullName, JsonConvert.SerializeObject(exTemplate, Formatting.Indented, new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects }));
                }
            }
            foreach (FileInfo fi in di.EnumerateFiles())
            {
                Templates.Add(JsonConvert.DeserializeObject<MarkupTemplate>(File.ReadAllText(fi.FullName), new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Objects }));
            }
        }

        private MarkupTemplate selectedTemplate;
        private ObservableCollection<MarkupTemplate> templates;

        public ObservableCollection<MarkupTemplate> Templates
        {
            get
            {
                return templates;
            }

            set
            {
                templates = value;
                OnPropertyChanged(() => Templates);
            }
        }

        public MarkupTemplate SelectedTemplate
        {
            get
            {
                return selectedTemplate;
            }

            set
            {
                selectedTemplate = value;
                OnPropertyChanged(() => SelectedTemplate);
            }
        }
    }
}
